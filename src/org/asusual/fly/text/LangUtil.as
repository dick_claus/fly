package org.asusual.fly.text {
    /**
     * @author Dmitry Bezverkhiy
     */
    public class LangUtil {
        
        /*
         * Return plurification of the word depending on number
         */
        public static function numberWord(number : int, word1 : String, word2 : String, word3 : String) : String {
            number %= 100;
            if (number > 10 && number < 20) {
                return word3;
            }
            number %= 10;
            switch (number) {
                case 1:
                    return word1;
                case 2:
                case 3:
                case 4:
                    return word2;
                default:
                    return word3;
            }
        }
        
        /*
         * Format number by groups of provided size
         */
         
         public static function formatNumber(number : Number, groupSize : int) : String {
         	var num : String = number.toString();
            var nums : Array = num.split('').reverse();
            var res : Array = [];
            for (var i : int = 0; i < nums.length; i++) {
                if (i!= 0 && i % (groupSize) == 0) {
                	res.push(' ');
                }
                res.push(nums[i]);
            } 
            return res.reverse().join('');
         }
        
        
    }
}
