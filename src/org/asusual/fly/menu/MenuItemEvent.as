package org.asusual.fly.menu {
    import flash.events.Event;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class MenuItemEvent extends Event {
        public static const ITEM_SELECTED : String = "ITEM_SELECTED";
        public var noEvent : Boolean;
        
        public function MenuItemEvent(type : String, noEvent : Boolean, bubbles : Boolean = false, cancelable : Boolean = false) {
            super(type, bubbles, cancelable);
            this.noEvent = noEvent;
        }
    }
}
