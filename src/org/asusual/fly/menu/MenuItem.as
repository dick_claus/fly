package org.asusual.fly.menu {
    import flash.events.MouseEvent;
    import flash.display.MovieClip;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class MenuItem extends MovieClip {
        protected var _id : String;
        protected var _selected : Boolean;

        public function MenuItem(id : String) {
            _id = id;
            addEventListener(MouseEvent.ROLL_OVER, onRollOverHandler);
            addEventListener(MouseEvent.ROLL_OUT, onRollOutHandler);
            addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
            this.buttonMode = true;
        }

		/*
		 * Select menu item on mouse down
		 */
        protected function onMouseDownHandler(event : MouseEvent) : void {
            select();
        }

		/*
		 * Show up state for menu item
		 */
        protected function onRollOutHandler(event : MouseEvent) : void {
            //should be overriden
        }

		/*
		 * Show hover state for menu item
		 */
        protected function onRollOverHandler(event : MouseEvent) : void {
            //should be overriden
        }

        /*
         * Getter for menu item id
         */
        public function get id() : String {
            return _id;
        }

        /*
         * Select current menu item
         */
        public function select(noEvent : Boolean = false) : void {
            if (!_selected) {
                _selected = true;
                dispatchEvent(new MenuItemEvent(MenuItemEvent.ITEM_SELECTED, noEvent));
            }
        }

		/*
		 * Getter for selected property
		 */
        public function get selected() : Boolean {
            return _selected;
        }

		/*
		 * Deselect menu item
		 */
        public function deselect() : void {
            _selected = false;
        }
        
        /*
         * Remove listeners on destroy
         */
        public function destroy() : void {
        	removeEventListener(MouseEvent.ROLL_OVER, onRollOverHandler);
            removeEventListener(MouseEvent.ROLL_OUT, onRollOutHandler);
            removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
        }
    }
}
