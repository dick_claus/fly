package org.asusual.fly.menu {
    import flash.display.MovieClip;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class Menu extends MovieClip {
        
        protected var items : Array = [];
        
        /*
         * Show menu for user
         */
        public function show() : void {
			//should be overriden        
        }
        
        /*
         * Hide menu from screen 
         */
        public function hide() : void {
        	//should be overriden
        }

		/*
		 * Add menu item to menu, and subscribe on select event.
		 * return array of menu items
		 */
        public function addItem(menuItem : MenuItem) : Array {
            menuItem.addEventListener(MenuItemEvent.ITEM_SELECTED, onItemSelectedHandler);
            items.push(menuItem);    
            return items;
        }
        
        /*
         * Remove all items from menu
         */
        public function clearMenu() : Array {
            while (items.length > 0) {
                var item : MenuItem = items.shift();
                onRemoveItem(item);
            }
        	return items;
        }

		/*
		 * Should be overriden for custom action on item remove
		 */
        protected function onRemoveItem(item : MenuItem) : void {
            item.removeEventListener(MenuItemEvent.ITEM_SELECTED, onItemSelectedHandler);
        }
        
        /*
         * Select menu item by id, if noEvent specified menu will not fire any event
         */
        public function select(itemID : String, noEvent : Boolean = false) : void {
        	var item : MenuItem = getItemById(itemID);
            item.select(noEvent);
        }
        
		/*
		 * Find and return item by id otherwise return null
		 */
        public function getItemById(itemID : String) : MenuItem {
            for (var i : int = 0; i < items.length; i++) {
                var item : MenuItem = items[i];
                if (item.id == itemID) {
                	return item;
                }
            }
            return null;
        }
        
        /*
         * Deselect all items
         */
        public function deselect() : void {
        	for (var i : int = 0; i < items.length; i++) {
        	    var item : MenuItem = items[i];
                item.deselect();
        	}
        }

		/*
		 * In item select handler we deselect other items and dispatch event if noEvent param was not passed
		 */
        protected function onItemSelectedHandler(event : MenuItemEvent) : void {
            var item : MenuItem;
			for (var i : Number = 0;i < items.length;i++) {
				item = items[i];
				if (event.target != item) {
					item.deselect();	
				}
			}
            var targetMenuItem : MenuItem = event.target as MenuItem;
            if (!event.noEvent) {
                dispatchEvent(new MenuEvent(MenuEvent.ON_SELECT_MENU_ITEM, targetMenuItem.id, targetMenuItem));
            }
        }
    }
}
