package org.asusual.fly.menu {
    import flash.events.Event;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class MenuEvent extends Event {
        public static const ON_SELECT_MENU_ITEM : String = "ON_SELECT_MENU_ITEM";
        public var id : String;
        public var item : MenuItem;
        
        public function MenuEvent(type : String, id : String, item : MenuItem, bubbles : Boolean = false, cancelable : Boolean = false) {
            super(type, bubbles, cancelable);
            this.id = id;
            this.item = item;
        }
    }
}
