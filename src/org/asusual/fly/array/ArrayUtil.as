package org.asusual.fly.array {
    import org.asusual.fly.math.Range;
    /**
     * ArrayUtil class is an all-static method class for array manipulations. You do not create instances of ArrayUtil; 
     * instead you call static methods such as ArrayUtil.copy()
     *  
     * @author Dmitry Bezverkhiy
     */
    public class ArrayUtil {	
        
        /**
         * Returns an array with randomly swapped elements of the original array.
         * 
         * <p>Implementation of Durstenfeld's version of Fisher–Yates shuffle algorithm,
         * that returns suffled array. Original array is not modified.
         * </p>
         * 
         * @param array The array that would be used as source for new shuffled array.
         * 
         * @return A new array that contains shuffled elements from the original array.
         */
        public static function shuffle(array : Array) : Array {
            var res : Array = [];
            var copy : Array = ArrayUtil.copy(array);
            while (copy.length > 1) {
                var arrayRange : Range = new Range(0,copy.length-1);
                var rnd : int = arrayRange.getRandomInt();
                res.push(copy[rnd]);
                copy.splice(rnd,1,copy[copy.length-1]);
                copy.length--;
            }
            res.push(copy[0]);
            return res;
        }

        /**
         * Returns simple copy of the array. 
         * 
         * @param array The array that would be copied. 
         * 
         * @return A new array that contains all the items from the original array.
         */
        public static function copy(array : Array) : Array {
            if (array == null) return new Array();
            var copy : Array = [];
            for (var i : int = 0; i < array.length; i++) {
                copy[i] = array[i];
            }
            return copy;
        }
    }
}
