package org.asusual.fly.math {
    /**
     * @author Dmitry Bezverkhiy
     */
    public class AngleUtil {
        
        public static function angleToRad(angle : Number) : Number {
            return angle/180*Math.PI;
        }
        
        public static function radToAngle(radians : Number) : Number {
            return radians/Math.PI*180;
        }
    }
}
