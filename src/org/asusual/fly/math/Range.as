package org.asusual.fly.math {
    /**
     * @author Dmitry Bezverkhiy
     */
    public class Range {
        
        private var low : Number;
        private var high : Number;
        
        public function Range(low : Number, high : Number) {
            this.low = Math.min(low, high);
            this.high = Math.max(low, high);
        }

        /* 
         * Return a random value from low to high, excluding high,
         * if point on unit interval is not specified
         * 
         */
        public function getRandomNumber(unitInterval : Number = NaN) : Number {
            if (isNaN(unitInterval) || unitInterval < 0 || unitInterval >= 1) { 
                unitInterval = Math.random();
            }
            return low + unitInterval*(high - low);
        }
        
        /* 
         * Return a random integer value from low to high inclusive,
         * if point on unit interval is not specified
         * 
         */
        public function getRandomInt(unitInterval : Number = NaN) : int {
            if (isNaN(unitInterval) || unitInterval < 0 || unitInterval > 1) { 
                unitInterval = Math.random();
            }
            if (low == high) {
            	return low;
            }
            return Math.ceil(low) + Math.round(unitInterval*(Math.floor(high) - Math.ceil(low)));
        }
        
        /*
         * Return a number from 0 to 1 that represents a position 
         * of given number between low and high 
         * return exception if the number is out of range
         */
        public function getFractionFromNumber(number : Number) : Number {
            if (number < low || number > high) {
            	throw(new RangeError(number +" is out of the range ["+low+","+high+"]"));
            }
            var scaleHigh : Number = high * 1000;
            var scaleLow : Number = low * 1000;
            var scaleNumber : Number = number * 1000;
            var fraction : Number = ((scaleNumber - scaleLow)/(scaleHigh - scaleLow));
        	return fraction;
        }
        
        /*
         * Return a number from low to high that represents 
         * of fraction of this range
         * return exception if the fraction is out of range 
         */
        public function getNumberFromFraction(fraction : Number) : Number {
        	if (fraction < 0 || fraction > 1) {
            	throw(new RangeError(fraction +" is out of the range ["+low+","+high+"]"));
            }
            var scaleHigh : Number = high*1000;
            var scaleLow : Number = low*1000;
            var scaleFraction : Number = fraction;
            var number : Number = (scaleLow + (scaleHigh - scaleLow)*scaleFraction)/1000;
        	return number;
        }

		/*
		 * Put the number inside the range
		 * 
		 */
        public function putInside(num : Number) : Number {
            if (num < low) {
            	num = low;
            } else if (num > high) {
            	num = high;
            }
            return num;
        }
    }
}
