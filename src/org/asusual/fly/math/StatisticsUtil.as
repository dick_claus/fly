package org.asusual.fly.math {
    /**
     * @author Dmitry Bezverkhiy
     */
    public class StatisticsUtil {
        
        /*
         * Get average from provided array 
         */
        public static function average(array : Array) : Number {
            return sum(array)/array.length;
        }
        
        /*
         * Get sum of all elements of the provided array 
         */
        public static function sum(array : Array) : Number {
            var sum : Number = 0;
            for (var i : int = 0; i < array.length; i++) {
                sum += array[i];
            }
        	return sum;
        }
    }
}
