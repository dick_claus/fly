package org.asusual.fly.bytearray
{
    /**
     * BitUtil is an utility class for bit manipulation
     *  
     * @author Dmitry Bezverkhiy
     */
    public class BitUtil
    {
        /**
         * Reads bit from source in provided position (zero-based)
         * 
         * @param source An uint number for read
         *   
         * @return A single bit from source number in specified position
         */
        public static function readBit(source : uint, position : uint) : uint
        {
            return uint(source & (1 << position)) >> position;
        }
        
        /**
         * Writes bit to soure to provided position (zero-based) without modifing source
         * Throw error on wrong bit value. Must be 1 or 0
         * return result uint
         */
        public static function writeBit(source : uint, position : uint, bitValue : uint) : uint
        {
            var result : uint;
            if (bitValue == 0)
            {
                result = source & ~(1 << position);
            }
            else if (bitValue == 1)
            {
                result = source | (1 << position);
            }
            else
            {
                throw new Error("BitValue should be 0 or 1");
            }
            return result;
        }
        
        /**
         * Flip bit in source in provided position (zero-based) without modifing source 
         * return result uint
         */
        public static function flipBit(source : uint, position : uint) : uint {
            var result : uint;
            result = source ^ (1 << position);
            return result;
        }
    }
}
