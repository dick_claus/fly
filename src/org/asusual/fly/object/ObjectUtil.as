package org.asusual.fly.object {
    import flash.utils.ByteArray;
    /**
     * ObjectUtil is an utility class for object manipulation
     *  
     * @author Dmitry Bezverkhiy
     */
    public class ObjectUtil {
        /**
         *  Copies the specified Object and returns a reference to the copy.
         *  The copy is made using a native serialization technique. 
         *   
         *  @param value Object that should be copied.
         * 
         *  @return Copy of the specified Object.
         */
        public static function copy(value : Object) : Object {
            var buffer : ByteArray = new ByteArray();
            buffer.writeObject(value);
            buffer.position = 0;
            var result : Object = buffer.readObject();
            return result;
        }
    }
}
