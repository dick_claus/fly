package org.asusual.fly.controls {
    import flash.display.BitmapData;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;
    import flash.text.TextFormat;
    import flash.text.TextField;
    import org.asusual.fly.controls.Element;

    /**
     * @author Dmitry.Bezverkhiy
     */
    public class Label extends Element {
        
        [Embed(source="/../assets/pf_easta_seven.ttf", fontFamily="Easta",fontName="Easta", embedAsCFF='false')]
        private var Easta : Class;
        
        private var textField : TextField;
        private var textFormat : TextFormat;
        private var _text : String = "";
        
        public function Label(text : String = null) {
            super();
            _id = "Label";
            initTextField();
            this.text = text;
        }

        private function initTextField() : void {
            textField = new TextField();
            textFormat = new TextFormat();
            textFormat.align = TextFormatAlign.LEFT;
            textFormat.font = "Easta";
            textFormat.size = 8;                    
            textField.defaultTextFormat = textFormat;
            textField.selectable = false;   
            textField.autoSize = TextFieldAutoSize.LEFT;        
            textField.embedFonts = true;
        }
        
        override protected function draw() : void {
            textField.text = _text;            
            bitmapData = new BitmapData(textField.width, textField.height,true,0);
            bitmapData.draw(textField);
            bitmap.bitmapData = bitmapData;
        }
        
        public function get text() : String {
            return _text;
        }

        public function set text(text : String) : void {
            if (text == null) {
                _text = "";
            } else {
                _text = text;
            }
            needUpdate();
        }
    }
}
