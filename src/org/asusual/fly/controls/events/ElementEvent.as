package org.asusual.fly.controls.events {
    import flash.events.Event;

    /**
     * @author Dmitry.Bezverkhiy
     */
    public class ElementEvent extends Event {
        public static const NEED_UPDATE : String = "needUpdate";
        
        public function ElementEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false) {
            super(type, bubbles, cancelable);
        }
    }
}
