package org.asusual.fly.controls {
    import flash.geom.Rectangle;
    import flash.display.BitmapData;
    /**
     * @author Dmitry.Bezverkhiy
     */
    public class InnerStatusBar extends Element {
        private var defaultWidth : Number;
        private var defaultHeight : Number;
        
        public function InnerStatusBar(width : Number, height : Number) {
            super();     
            _id = "InnerStatusBar";
            defaultWidth = width;
            defaultHeight = height;
        }
        
        
        override protected function draw() : void {            
            bitmapData = new BitmapData(defaultWidth, defaultHeight, true, 0);
            bitmapData.lock();
            bitmapData.fillRect(new Rectangle(0,0,defaultWidth, defaultHeight), 0xFF666666);
            bitmapData.fillRect(new Rectangle(1,1,defaultWidth-2, defaultHeight-2), 0xFFCCCCCC);
            bitmapData.fillRect(new Rectangle(0,defaultHeight-1,defaultWidth,1), 0xFFFFFFFF);
            bitmapData.fillRect(new Rectangle(defaultWidth-1,0,1,defaultHeight), 0xFFFFFFFF);
            bitmapData.unlock();
            bitmap.bitmapData = bitmapData;
        }
        
                
        override public function set width(value : Number) : void {            
            defaultWidth = value;
            needUpdate();
        }
        
        override public function set height(value : Number) : void {
            defaultHeight = value;
            needUpdate();
        }
        
        override protected function drawChildren() : void {
            super.drawChildren();
            
        }
    }
}
