package org.asusual.fly.controls {
    import flash.geom.Matrix;
    import flash.display.BitmapData;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

	/**
	 * @author Dmitry.Bezverkhiy
	 */
	public class Button extends Element {			
		private static const SIDE_MARGIN : Number = 20;
		private static const TOP_MARGIN : Number = 10;
		
        private var _label : Label;		
		private var bitmapDataUp : BitmapData;
		private var bitmapDataDown : BitmapData;

		public function Button(labelText : String) {
            super();
            _id = "Button";
			initLabel();
            _label.text = labelText;
            addEventListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage,false, 0, true);
        }

        private function initLabel() : void {
            _label = new Label();            
        }

        private function onRemoveFromStage(event : Event) : void {
            removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
        }   

        override protected function onAddedToStage(event : Event) : void {
            super.onAddedToStage(event);
            addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);	
        }
		
		public function get labelText() : String {
			return _label.text;
		}

		public function set labelText(labelText : String) : void {
			_label.text = labelText;
			needUpdate();
		}

		private function onMouseUp(event : MouseEvent) : void {
            bitmapData = bitmapDataUp;		
            bitmap.bitmapData = bitmapDataUp;
		}

		private function onMouseDown(event : MouseEvent) : void {
            bitmapData = bitmapDataDown;
			bitmap.bitmapData = bitmapDataDown;
		}	

		override protected function draw() : void {						
			createBitmap(_label.width + SIDE_MARGIN, _label.height + TOP_MARGIN);
		}
        
        override protected function buildChildren() : void {
            _label.build();                                              
        }

		private function createBitmap(w : Number, h : Number) : void {
			if (w < 80) {
				w = 80;
			}
			bitmapDataUp = new BitmapData(w, h, true,0);
			bitmapDataDown = new BitmapData(w, h, true);
			
			drawButton(bitmapDataUp, w, h,0xFFFFFFFF,0xFF666666, 0);
			drawButton(bitmapDataDown, w, h, 0xFF666666,0xFFFFFFFF, 1);
			
            bitmapData = bitmapDataUp;
			bitmap.bitmapData = bitmapData;
		}

		private function drawButton(bitmapData : BitmapData, w : Number, h : Number, hiColor : uint, lowColor : uint, shift: Number) : void {
			bitmapData.lock();
			bitmapData.fillRect(new Rectangle(1,0,w-2,1), 0xFF000000);
			bitmapData.fillRect(new Rectangle(0,1,1,h-2), 0xFF000000);
			bitmapData.fillRect(new Rectangle(w-1,1,1,h-2), 0xFF000000);
			bitmapData.fillRect(new Rectangle(1,h-1,w-2,1), 0xFF000000);
			bitmapData.fillRect(new Rectangle(1,1,w-2,1), hiColor);
			bitmapData.fillRect(new Rectangle(1,1,1,h-2), hiColor);
			bitmapData.fillRect(new Rectangle(1,2,w-3,1), hiColor);
			bitmapData.fillRect(new Rectangle(2,1,1,h-3), hiColor);
			bitmapData.fillRect(new Rectangle(w-2,2,1,h-3), lowColor);
			bitmapData.fillRect(new Rectangle(2,h-2,w-3,1), lowColor);
			bitmapData.fillRect(new Rectangle(w-3,3,1,h-4), lowColor);
			bitmapData.fillRect(new Rectangle(3,h-3,w-4,1), lowColor);
			bitmapData.fillRect(new Rectangle(3,3,w-6,h-6), 0xFFCCCCCC);	
            bitmapData.draw(_label,new Matrix(1,0,0,1,Math.floor((w - _label.width)/2), Math.floor((h - _label.height)/3)+shift));		
			bitmapData.unlock();
		}
	}
}
