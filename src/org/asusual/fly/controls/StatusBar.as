package org.asusual.fly.controls {
    import flash.display.BitmapData;
    import flash.geom.Rectangle;

    /**
     * @author Dmitry.Bezverkhiy
     */
    public class StatusBar extends Element {
        private var defaultWidth : Number;
        private var defaultHeight : Number;
        
        public function StatusBar(width : Number, height : Number) {
            super();     
            _id = "StatusBar";
            defaultWidth = width;
            defaultHeight = height;
        }
        
        override protected function draw() : void {
            bitmapData = new BitmapData(defaultWidth, defaultHeight, true, 0);
            bitmapData.lock();
            bitmapData.fillRect(new Rectangle(0,0,defaultWidth, defaultHeight), 0xFF000000);
            bitmapData.fillRect(new Rectangle(1,1,defaultWidth-2, defaultHeight-2), 0xFFCCCCCC);
            bitmapData.unlock();
            bitmap.bitmapData = bitmapData;
        }        
    }
}
