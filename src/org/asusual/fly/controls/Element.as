package org.asusual.fly.controls {
    import org.asusual.fly.controls.events.ElementEvent;

    import flash.geom.Matrix;
    import flash.display.Stage;
    import flash.events.Event;
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Sprite;

    /**
     * @author Dmitry.Bezverkhiy
     */
    public class Element extends Sprite {
        private static var elementList : Array = [];
        private static var _stage : Stage;

        public static function initUpdate(stage : Stage) : void {
            _stage = stage;
            _stage.addEventListener(Event.RENDER, update);
        }

        private static function update(event : Event) : void {            
            while (elementList.length > 0) {
                var element : Element = Element(elementList.pop());
                if (element.dirty) {
                    element.build();
                }
            }
        }

        protected var _id : String;
        protected var bitmap : Bitmap;
        protected var bitmapData : BitmapData;
        protected var elements : Array = [];
        protected var _dirty : Boolean;

        public function Element() {
            _dirty = true;
            elementList.push(this);
            initBitmap();
            addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
        }

        public final function needUpdate() : void {
            if (!dirty) {
                elementList.push(this);
                _dirty = true;
                if (!stage) {
                    dispatchEvent(new ElementEvent(ElementEvent.NEED_UPDATE));
                } else {
                    if (_stage) {                        
                        _stage.invalidate();
                    }
                }
            }
        }

        protected function onAddedToStage(event : Event) : void {
            if (dirty) {
                if (!Element.stage) initUpdate(stage);
                stage.invalidate();
            }
        }

        protected function initBitmap() : void {
            bitmapData = new BitmapData(10, 10, true);
            bitmap = new Bitmap();
            bitmap.bitmapData = bitmapData;
            addChild(bitmap);
        }

        public function build() : void {
            buildChildren();
            draw();
            drawChildren();
            dirty = false;
        }

        protected function draw() : void {
            // for subclasses to override
        }

        protected function buildChildren() : void {
            for (var i : int = 0; i < elements.length; i++) {
                var element : Element = elements[i];
                element.build();
            }
        }

        protected function drawChildren() : void {
            for (var i : int = 0; i < elements.length; i++) {
                var element : Element = elements[i];
                var matrix : Matrix = new Matrix(1, 0, 0, 1, element.x, element.y);
                bitmapData.draw(element, matrix);
                bitmap.bitmapData = bitmapData;
            }
        }

        private function onNeedUpdate(event : ElementEvent) : void {
            needUpdate();
        }

        public function addElement(element : Element) : void {
            elements.push(element);
            element.addEventListener(ElementEvent.NEED_UPDATE, onNeedUpdate);
        }

        public function removeElement(element : Element) : void {
            for (var i : int = 0; i < elements.length; i++) {
                var currentElement : Element = elements[i];
                if (currentElement == element) {
                    element.removeEventListener(ElementEvent.NEED_UPDATE, onNeedUpdate);
                    elements.splice(i, 1);
                    break;
                }
            }
        }

        public function get dirty() : Boolean {
            return _dirty;
        }

        public function set dirty(dirty : Boolean) : void {
            _dirty = dirty;
        }

        static public function get stage() : Stage {
            return _stage;
        }

        public function get id() : String {
            return _id;
        }
    }
}
