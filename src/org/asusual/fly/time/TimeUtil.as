package org.asusual.fly.time {
import org.asusual.fly.text.LangUtil;

public class TimeUtil {

        public static const SECOND_MS : Number = 1 * 1000;
        public static const MINUTE_MS : Number = 60 * SECOND_MS;
        public static const HOUR_MS : Number = 60 * MINUTE_MS;
        public static const DAY_MS : Number = 24 * HOUR_MS;
        public static const WEEK_MS : Number = 7 * DAY_MS;


        public static function getDaysBack(timeMs : Number, nowMs : Number):String
        {
            var daysBack:String;
            var days : Number = Math.floor((nowMs - timeMs)/DAY_MS);
            if (days < 0) {
                throw new Error('Введеный интервал лежит в будущем');
            }
            if (days > 1) {
                var numberWord:String = LangUtil.numberWord(days, "день", "дня", "дней");
                daysBack = days+' '+numberWord+' '+"назад";
            } else if (days == 1) {
                daysBack = "вчера";
            } else if (days == 0) {
                daysBack = "сегодня";
            }
            return daysBack;
        }
    }
}
