package org.asusual.fly.color
{
    import flash.geom.ColorTransform;
    import flash.display.DisplayObject;

    /**
     * ColorUtil class is an unitility class that helps to work with colors
     * 
     * @author Dmitry Bezverkhiy
     */
    public class ColorUtil
    {
        public static function extractRedChannel(color:uint):uint
        {
            return color >> 16;
        }

        public static function extractGreenChannel(color:uint):uint
        {
            return (color >> 8) & 0xFF;
        }

        public static function extractBlueChannel(color:uint):uint
        {
            return color & 0xFF;
        }
        
        public static function tint(displayObject : DisplayObject, color : uint) : void {
            var colorTransform : ColorTransform = new ColorTransform();
            colorTransform.color = color;
            displayObject.transform.colorTransform = colorTransform; 
        }
        
        public static function rbgToHex(r : int, g : int, b : int) : uint {
        	return r << 16 | g << 8 | b;
        }
    }
}
