package org.asusual.fly.web {
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.display.Sprite;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class WebSprite extends Sprite {
        public function WebSprite() {
            if (stage) {
                onAddedToStageHandler();
            } else {
                addEventListener(Event.ADDED_TO_STAGE, onAddedToStageHandler);
            }
        }

        protected function onAddedToStageHandler(event : Event = null) : void {
            removeEventListener(Event.ADDED_TO_STAGE, onAddedToStageHandler);
            stage.frameRate = 31;
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
        }
    }
}
