package org.asusual.fly.vector {
    import org.asusual.fly.math.Range;

    import flash.system.ApplicationDomain;
    import flash.utils.getQualifiedClassName;

    import org.asusual.fly.vector.VectorUtil;

    /**
     * VectorUtil class is an all-static method class for vector manipulations. You do not create instances of ArrayUtil;
     * instead you call static methods such as ArrayUtil.copy()
     *
     * @author Dmitry Bezverkhiy
     */
    public class VectorUtil {
        /*
         * Implementation of Durstenfeld's version of Fisher–Yates shuffle algorithm,
         * return the vector with randomly swapped elements of the original vector.
         * Original vector is not modified. 
         */
        public static function shuffle(vector : *) : * {
            var res : * = getCustomVector(vector);
            var copy : * = VectorUtil.copy(vector);
            while (copy.length > 1) {
                var vectorRange : Range = new Range(0, copy.length - 1);
                var rnd : int = vectorRange.getRandomInt();
                res.push(copy[rnd]);
                copy.splice(rnd, 1, copy[copy.length - 1]);
                copy.length--;
            }
            res.push(copy[0]);
            return res;
        }

        /*
         * Returns simple copy of the vector. 
         */
        public static function copy(vector : *) : * {
            if (vector == null) return getCustomVector(vector);
            var copy : * = getCustomVector(vector);
            for (var i : int = 0; i < vector.length; i++) {
                copy[i] = vector[i];
            }
            return copy;
        }

        private static const VECTOR_CLASS_NAME : String = getQualifiedClassName(Vector);

        private static function getCustomVector(vector : *, applicationDomain : ApplicationDomain = null) : * {
            if (!applicationDomain) applicationDomain = ApplicationDomain.currentDomain;
            var itemDefinition : *;
			if (vector == null) {
                return new Vector.<*>;
            } else {
                itemDefinition = vector[0];
            }
            var C : Class = applicationDomain.getDefinition(VECTOR_CLASS_NAME + '.<' + getQualifiedClassName(itemDefinition) + '>') as Class;
            return new C();
        }
    }
}
