package org.asusual.fly.screen {
    /**
     * @author Dmitry Bezverkhiy
     */
    public class ScreenUtilScaleMode {
        
        public static const LETTERBOX : String = "letterbox";
        public static const ZOOM : String = "zoom";
    }
}
