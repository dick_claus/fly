package org.asusual.fly.screen {
    import flash.events.Event;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class ScreenEvent extends Event {
        public static const CHANGE_SCREEN : String = "changeScreen";
        
        public var screenId : String;
        
        public function ScreenEvent(type : String, screenId : String, bubbles : Boolean = false, cancelable : Boolean = false) {
            this.screenId = screenId;
            super(type, bubbles, cancelable);
        }
    }
}
