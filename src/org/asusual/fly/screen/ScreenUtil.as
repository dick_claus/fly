package org.asusual.fly.screen {
    import flash.display.DisplayObject;
    import flash.display.Stage;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class ScreenUtil {
        public static function centerToScreen(screen : DisplayObject, displayObject : DisplayObject) : void {
            var posX : Number = 0;
            var posY : Number = 0;
            if (screen is Stage) {
                posX = ((screen as Stage).stageWidth - displayObject.width) / 2;
                posY = ((screen as Stage).stageHeight - displayObject.height) / 2;
            } else {
                posX = (screen.width - displayObject.width) / 2;
                posY = (screen.height - displayObject.height) / 2;
            }
            displayObject.x = posX;
            displayObject.y = posY;
        }

        public static function fitToScreen(screen : DisplayObject, displayObject : DisplayObject, scaleMode : String = "letterbox") : void {
            var newW : Number = 0;
            var newH : Number = 0;
            if (screen is Stage) {
                var screenWidth : Number = (screen as Stage).stageWidth;
                var screenHeight : Number = (screen as Stage).stageHeight;
            } else {
                screenWidth = screen.width;
                screenHeight = screen.height;
            }
            var ratioWidth : Number = displayObject.width / screenWidth;
            var ratioHeight : Number = displayObject.height / screenHeight;
            var ratio : Number = displayObject.width / displayObject.height;

            if (ratioWidth > ratioHeight && scaleMode == ScreenUtilScaleMode.LETTERBOX || ratioWidth < ratioHeight && scaleMode == ScreenUtilScaleMode.ZOOM) {
                newW = screenWidth;
                newH = newW/ratio;
            } else {
                newH = screenHeight;
                newW = newH * ratio;
            }

            displayObject.width = Math.round(newW);
            displayObject.height = Math.round(newH);
        }

        public static function alignVerticalCenter(screen : DisplayObject, alignee : DisplayObject) : void {
            var posY : Number = 0;
            if (screen is Stage) {
                posY = ((screen as Stage).stageHeight - alignee.height) / 2;
            } else {
                posY = (screen.height - alignee.height) / 2;
            }
            alignee.y = posY;
        }

        public static function alignHorizontalCenter(screen : DisplayObject, alignee : DisplayObject) : void {
            var posX : Number = 0;
            if (screen is Stage) {
                posX = ((screen as Stage).stageWidth - alignee.width) / 2;
            } else {
                posX = (screen.width - alignee.width) / 2;
            }
            alignee.x = posX;
        }
    }
}
