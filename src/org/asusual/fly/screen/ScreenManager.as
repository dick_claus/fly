package org.asusual.fly.screen {
    import flash.display.Sprite;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class ScreenManager extends Sprite {
        private var screens : Object = {};
        private var activeScreen : Screen;
        
        public function ScreenManager() {
            activeScreen = null;
        }
        
        /**
         * Registers class for later instanstiation
         * Class should be subclass of Screen class
         */

        public function registerScreen(screenClass : Class, screenId : String) : Object {
            screens[screenId] = screenClass;
            return screens;
        }

		/**
		 * Change screen by id provided on registration
		 */
        public function changeScreen(screenId : String) : void {
            if (activeScreen != null) {
                destroyActiveScreen();
            }
            var ScreenClass : Class = screens[screenId];
            var screen : Screen = new ScreenClass();
            activeScreen = screen;
            activeScreen.addEventListener(ScreenEvent.CHANGE_SCREEN, onChangeScreenHandler);
            addChild(activeScreen);
        }

        private function destroyActiveScreen() : void {
            activeScreen.removeEventListener(ScreenEvent.CHANGE_SCREEN, onChangeScreenHandler);
            activeScreen.destroy();
            removeChild(activeScreen);
            activeScreen = null;
        }
        
        /**
         * Return the active screen
         */
        public function getActiveScreen() : Screen {
        	return activeScreen;
        }

        private function onChangeScreenHandler(event : ScreenEvent) : void {
            changeScreen(event.screenId);
        }
    }
}
