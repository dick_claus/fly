package org.asusual.fly.screen {
    import flash.display.Sprite;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class Screen extends Sprite {
        public function Screen() {
        }
        
        protected function changeScreen(screenId : String) : void {
            dispatchEvent(new ScreenEvent(ScreenEvent.CHANGE_SCREEN, screenId));
        }
        
        /**
         * Method to show a screen.
         */
        public function show() : void {
            // should be overriden
        }
        
        /**
         * Method to hide a screen.
         */
        public function hide() : void {
        	// should be overriden
        }
        
        /**
         * Method to destroy screen 
         */
        public function destroy() : void {
        	// should be overriden
        }
    }
}
