package org.asusual.fly.graphics {
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class CircleUtil {
        
        /*
         * Draw a sector in container given of the radius given
         * Endtime is a point on unit interval where 0 is empty and 1 is full circle 
         * Step is used for smooth circles.
         */
        
        public static function drawSector(container : DisplayObject, radius : Number, endTime : Number, step : Number = 0.03) : void {
            if (endTime > 1 || endTime < 0) throw new Error("End time is point of unit interval from 0 to 1 inclusive");
            if (step < 0) throw new Error("Step should be greater than 0");
            var centerPoint : Point = new Point();
            if (container is Sprite) {
                var spr : Sprite = container as Sprite;
                spr.graphics.clear();
                spr.graphics.beginFill(0xFF0000);
                spr.graphics.moveTo(centerPoint.x, centerPoint.y);
                for (var i : Number = 0; i < endTime; i+=step) {
                    var newX : Number = centerPoint.x + radius*Math.cos(i*2*Math.PI);
                    var newY : Number = centerPoint.y + radius*Math.sin(i*2*Math.PI);
                    spr.graphics.lineTo(newX, newY);
                }
                spr.graphics.lineTo(centerPoint.x, centerPoint.y);
                spr.graphics.endFill();
            }
        }
    }
}
