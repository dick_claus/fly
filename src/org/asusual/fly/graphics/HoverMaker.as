package org.asusual.fly.graphics {
    import flash.display.InteractiveObject;
    import flash.events.MouseEvent;
    import flash.utils.Dictionary;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class HoverMaker {
        
        private static var dict : Dictionary = new Dictionary();
        
        public function HoverMaker() {
        }
        
        /**
         * Register interactive object to be transformed on roll over and roll out.
         */
        public static function register(interactiveObject : InteractiveObject) : Boolean {
            if (dict[interactiveObject] != null) {
            	return false;
            }
            dict[interactiveObject] = interactiveObject;
            interactiveObject.addEventListener(MouseEvent.ROLL_OVER, onMouseOverHandler);
            interactiveObject.addEventListener(MouseEvent.ROLL_OUT, onMouseOutHandler);
            return true;
        }
        
        /**
         * Register interactive object to be transformed on roll over and roll out.
         */
        public static function unregister(interactiveObject : InteractiveObject) : Boolean {
            if (dict[interactiveObject] != null) {
            	var obj : InteractiveObject = dict[interactiveObject];
                obj.removeEventListener(MouseEvent.ROLL_OVER, onMouseOverHandler);
                obj.removeEventListener(MouseEvent.ROLL_OUT, onMouseOutHandler);
                delete dict[interactiveObject];
                return true;
            }
            return false;
        }
		
        private static function onMouseOutHandler(event : MouseEvent) : void {
            transform(event.target as InteractiveObject);             
        }
        
        private static function onMouseOverHandler(event : MouseEvent) : void {
            resetTransform(event.target as InteractiveObject);
        }

        protected static function resetTransform(target : InteractiveObject) : void {
            // should be overriden
        }

        protected static function transform(target : InteractiveObject) : void {
            // should be overriden
        }

    }
}
