package org.asusual.fly.interactive {
    import flash.display.Stage;

    import org.asusual.fly.math.Range;

    import flash.display.InteractiveObject;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    /**
     * @author Anton Sidorenko
     * modified by Dmitry Bezverkhiy
     */
    public class ScrollController extends EventDispatcher {
        private var eventDispatcher : EventDispatcher;
        protected var _target : InteractiveObject;
        protected var _bounds : Rectangle;
        protected var xRange : Range;
        protected var yRange : Range;
        protected var stage : Stage;
        protected var _isDragging : Boolean;
        protected var pressPoint : Point;
        protected var _xFraction : Number;
        protected var _yFraction : Number;

        public function ScrollController(target : InteractiveObject, bounds : Rectangle, eventDispatcher : EventDispatcher = null) {
            _target = target;
            this.eventDispatcher = eventDispatcher || target;
            this.bounds = bounds;
        }

        public function get target() : InteractiveObject {
            return _target;
        }

        public function get bounds() : Rectangle {
            return _bounds;
        }

        public function set bounds(bounds : Rectangle) : void {
            this._bounds = bounds;
            update();
        }

        public function enable() : void {
            eventDispatcher.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
        }

        public function disable() : void {
            eventDispatcher.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
            if (stage) {
                stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
                stage.removeEventListener(Event.MOUSE_LEAVE, onMouseUpHandler);
                stage.removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
            }
        }

        public function correctPosition() : void {
            if (bounds) {
                target.x = xRange.putInside(target.x);
                target.y = yRange.putInside(target.y);
            }
        }

        public function destroy() : void {
            onMouseUpHandler(null);
            disable();
            _target = null;
            stage = null;
            _bounds = null;
            xRange = null;
            yRange = null;
            _xFraction = 0;
            _yFraction = 0;
        }

        public function get isDragging() : Boolean {
            return _isDragging;
        }

        public function get xFraction() : Number {
            return _xFraction;
        }

        public function set xFraction(xFraction : Number) : void {
            _xFraction = Math.max(Math.min(1, xFraction), 0);
            target.x = xRange.getNumberFromFraction(_xFraction);
        }

        public function get yFraction() : Number {
            return _yFraction;
        }

        public function set yFraction(yFraction : Number) : void {
            _yFraction = Math.max(Math.min(1, yFraction), 0);
            target.y = yRange.getNumberFromFraction(_yFraction);
        }

        protected function onMouseDownHandler(event : Event) : void {
            _isDragging = true;

            stage = target.stage;
            stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
            stage.addEventListener(Event.MOUSE_LEAVE, onMouseUpHandler);
            stage.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);

            pressPoint = new Point(target.mouseX, target.mouseY);

            dispatchEvent(new ScrollControllerEvent(ScrollControllerEvent.ON_DRAG_START));
        }

        protected function onMouseUpHandler(event : Event) : void {
            _isDragging = false;
            pressPoint = null;
            if (stage) {
                stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
                stage.removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
                stage.removeEventListener(Event.MOUSE_LEAVE, onMouseUpHandler);
            }
            dispatchEvent(new ScrollControllerEvent(ScrollControllerEvent.ON_DRAG_STOP, _xFraction, _yFraction));
        }

        protected function onEnterFrameHandler(event : Event) : void {
            var coordinateInParent : Point = target.parent.globalToLocal(new Point(stage.mouseX, stage.mouseY));

            target.x = coordinateInParent.x - pressPoint.x * target.scaleX;
            target.y = coordinateInParent.y - pressPoint.y * target.scaleY;

            correctPosition();
            if (xRange) {
                _xFraction = xRange.getFractionFromNumber(target.x);
            } else {
                _xFraction = NaN;
            }
            if (yRange) {
                _yFraction = yRange.getFractionFromNumber(target.y);
            } else {
                _yFraction = NaN;
            }

            dispatchEvent(new ScrollControllerEvent(ScrollControllerEvent.ON_DRAG_PROGRESS, _xFraction, _yFraction));
        }

        private function update() : void {
            if (bounds) {
                xRange = new Range(bounds.left, bounds.right);
                yRange = new Range(bounds.top, bounds.bottom);
            } else {
                xRange = null;
                yRange = null;
            }
        }
    }
}
