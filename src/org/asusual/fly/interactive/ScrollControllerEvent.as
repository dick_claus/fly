package org.asusual.fly.interactive {
    import flash.events.Event;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class ScrollControllerEvent extends Event {
        public static const ON_DRAG_START : String = "ON_DRAG_START";
        public static const ON_DRAG_PROGRESS : String = "ON_DRAG_PROGRESS";
        public static const ON_DRAG_STOP : String = "ON_DRAG_STOP";
        public var xFraction : Number;
        public var yFraction : Number;
        
        public function ScrollControllerEvent(type : String,_xFraction:Number = 0, _yFraction:Number = 0, bubbles : Boolean = false, cancelable : Boolean = false) {
            super(type, bubbles, cancelable);
            xFraction = _xFraction;
            yFraction = _yFraction;
        }
    }
}
