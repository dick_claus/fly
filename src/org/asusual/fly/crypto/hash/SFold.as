package org.asusual.fly.crypto.hash {
    /**
     * @author Dmitry Bezverkhiy
     */
    public class SFold {
        /* 
         * Return a hash from string
         * 
         */
        public static function hash(str : String) : Number {
        	var len : int = str.length / 4;
  			var sum : Number = 0;
            var curr : int = 0;            
  			for (var j : int = 0; j < len; j++) {
		    	var mult : int = 1;
		    	for (var k : int = 0 ; k < 4; k++) {
		    		sum += str.charCodeAt(curr) * mult;
		    		curr++;
		    		mult *= 256;
		    	}
		    }	
            
            mult = 1;
    		while (curr < str.length) {
      			sum += str.charCodeAt(curr) * mult;
      			curr++;
      			mult *= 256;
    		}	  	
		
		  	return sum;		    
         }
    }
}
