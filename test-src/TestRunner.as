package {
    import org.asusual.fly.object.ObjectUtilTest;
    import org.asusual.fly.math.test.StatisticsUtilTest;
    import org.asusual.fly.array.test.ArrayUtilTest;
    import org.asusual.fly.bytearray.test.BitUtilTest;
    import org.asusual.fly.color.test.ColorUtilTest;
    import org.asusual.fly.controls.test.ButtonTest;
    import org.asusual.fly.controls.test.LabelTest;
    import org.asusual.fly.graphics.test.CircleUtilTest;
    import org.asusual.fly.graphics.test.HoverMakerTest;
    import org.asusual.fly.math.test.AngleUtilTest;
    import org.asusual.fly.math.test.RangeTest;
    import org.asusual.fly.menu.test.MenuItemTest;
    import org.asusual.fly.menu.test.MenuTest;
    import org.asusual.fly.screen.test.ScreenEventTest;
    import org.asusual.fly.screen.test.ScreenManagerTest;
    import org.asusual.fly.screen.test.ScreenUtilTest;
    import org.asusual.fly.text.test.LangUtilTest;
    import org.asusual.fly.time.test.TimeUtilTest;
    import org.asusual.fly.crypto.hash.test.SFoldTest;
    import org.asusual.fly.utils.test.GUIDTest;
    import org.asusual.fly.vector.test.VectorUtilTest;
    import org.asusual.fly.web.test.WebSpriteTest;
    import org.flexunit.internals.TraceListener;
    import org.flexunit.runner.FlexUnitCore;
    import org.fluint.uiImpersonation.VisualTestEnvironmentBuilder;

    import flash.display.Sprite;

    [SWF(backgroundColor="#FFFFFF", frameRate="31", width="640", height="480")]
    public class TestRunner extends Sprite
    {
        private var core:FlexUnitCore;

        public function TestRunner()
        {
            // Launch your unit tests by right clicking within this class and select: Debug As > FDT SWF Application
			
            // Instantiate the core.
            core = new FlexUnitCore();
            // This registers the stage with the VisualTestEnvironmentBuilder, which allows you
            // to use the UIImpersonator classes to add to the display list and remove from within your
            // tests. With Flex, this is done for you, but in AS projects it needs to be handled manually.
            VisualTestEnvironmentBuilder.getInstance(this);

            // Add any listeners. In this case, the TraceListener has been added to display results.
            core.addListener(new TraceListener());

            // There should be only 1 call to run().
            // You can pass a comma separated list (or an array) of tests or suites.
            // You can also pass a Request Object, which allows you to sort, filter and subselect.
            // var request:Request = Request.methods( someClass, ["method1", "method2", "method3"] ).sortWith( someSorter ).filterWith( someFilter );
            // core.run( request );
            var tests:Array = [];
            tests.push(BitUtilTest);
            tests.push(SFoldTest);
            tests.push(ColorUtilTest);
            tests.push(LangUtilTest);
            tests.push(ButtonTest);
            tests.push(AngleUtilTest);
            tests.push(RangeTest);
            tests.push(StatisticsUtilTest);
            tests.push(CircleUtilTest);
            tests.push(WebSpriteTest);
            tests.push(ObjectUtilTest);
            tests.push(ArrayUtilTest);
            tests.push(VectorUtilTest);
            tests.push(LabelTest);
            tests.push(ScreenUtilTest);
            tests.push(ScreenManagerTest);
            tests.push(ScreenEventTest);
            tests.push(HoverMakerTest);
            tests.push(GUIDTest);
            tests.push(MenuTest);
            tests.push(MenuItemTest);
            tests.push(TimeUtilTest);


            core.run(tests);
        }
    }
}
