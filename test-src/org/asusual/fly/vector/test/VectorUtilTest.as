package org.asusual.fly.vector.test {
    import flexunit.framework.Assert;

    import org.asusual.fly.vector.VectorUtil;
    import org.hamcrest.assertThat;
    import org.hamcrest.collection.hasItem;
    import org.hamcrest.core.allOf;
    import org.hamcrest.number.closeTo;
    import org.hamcrest.object.equalTo;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class VectorUtilTest {
        
        [Test]
        public function testShuffle() : void
        {
            var input : Vector.<int> = new <int>[1,2,3,4,5,6,7,8];
            var output : Vector.<int> = VectorUtil.shuffle(input) as Vector.<int>;
            assertThat(output, allOf(hasItem(equalTo(3)), hasItem(closeTo(1, 8))));
        }
        
        [Test]
        public function testCopyForNull() : void
        {
            var input : Vector.<*> = null;
            var output : Vector.<*> = VectorUtil.copy(input);
            Assert.assertNotNull(output);
        }
        
        [Test]
        public function testCopyForEqual() : void
        {
            var input : Vector.<int> = new <int>[1,2,3,4,5,6,7,8];
            var output : Vector.<int> = VectorUtil.copy(input);
            assertThat(output[0], equalTo(1));
        }
        
        [Test]
        public function testCopyForEqualString() : void
        {
            var input : Vector.<String> = new <String>["apple","banana","cake"];
            var output : Vector.<String> = VectorUtil.copy(input);
            assertThat(output[0], equalTo("apple"));
        }
        
        
    }
}
