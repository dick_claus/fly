package org.asusual.fly.crypto.hash.test {
    
    import flexunit.framework.Assert;
    import org.asusual.fly.crypto.hash.SFold;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class SFoldTest {
        
        
        [Test]
        public function testHash1() : void
        {
            var input : String = "probability";
            var hash : Number = SFold.hash(input);            
            Assert.assertEquals(3478276410, hash);
        }
        
        [Test]
        public function testHash2() : void
        {
            var input : String = "a";
            var hash : Number = SFold.hash(input);            
            Assert.assertEquals(97, hash);
        }
        
        [Test]
        public function testHash3() : void
        {
            var input : String = "ff";
            var hash : Number = SFold.hash(input);            
            Assert.assertEquals(26214, hash);
        }
        
        [Test]
        public function testHash4() : void
        {
            var input : String = "777";
            var hash : Number = SFold.hash(input);            
            Assert.assertEquals(3618615, hash);
        }
        
        [Test]
        public function testHash5() : void
        {
            var input : String = "fall over the hedge";
            var hash : Number = SFold.hash(input);            
            Assert.assertEquals(6981712065, hash);
        }
    }
}
