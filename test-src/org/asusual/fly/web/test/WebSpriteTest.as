package org.asusual.fly.web.test {
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import org.fluint.uiImpersonation.UIImpersonator;
    import org.flexunit.Assert;
    import org.asusual.fly.web.WebSprite;
    import flash.display.Sprite;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class WebSpriteTest extends Sprite {
        private var webSprite : WebSprite;
        
        [Before]
        public function beforeTest() : void {
            webSprite = new WebSprite();
            UIImpersonator.addChild(webSprite);
        }
        
        [Test(async)]
        public function testFPS():void
        {
            Assert.assertEquals(webSprite.stage.frameRate, 31);
        }
        
        [Test(async)]
        public function testScale():void
        {
            Assert.assertEquals(webSprite.stage.scaleMode, StageScaleMode.NO_SCALE);
        }
        
        [Test(async)]
        public function testAlign():void
        {
            Assert.assertEquals(webSprite.stage.align, StageAlign.TOP_LEFT);
        }
    }
}
