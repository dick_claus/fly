// Copyright 2012. Art. Lebedev Studio. All Rights Reserved.
package org.asusual.fly.bytearray.test
{
    import org.asusual.fly.bytearray.BitUtil;
    import org.flexunit.Assert;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class BitUtilTest
    {
        public var source : uint;
        public var position : uint;
        public var bitValue : uint;

        [Test]
        public function testReadBit() : void
        {
            source = 0xff;
            position = 2;
            Assert.assertEquals(1, BitUtil.readBit(source, position));
            source = 0x05;
            position = 1;
            Assert.assertEquals(0, BitUtil.readBit(source, position));
        }

        [Test]
        public function testWriteBitOne() : void
        {
            source = 0x05;
            position = 1;
            bitValue = 1;
            Assert.assertEquals(0x07, BitUtil.writeBit(source, position, bitValue));
        }

        [Test]
        public function testWriteBitZero() : void
        {
            source = 0x0f;
            position = 2;
            bitValue = 0;
            Assert.assertEquals(0x0B, BitUtil.writeBit(source, position, bitValue));
        }

        [Test(expects="Error")]
        public function testWriteBitWithException() : void
        {
            source = 0x0f;
            position = 3;
            bitValue = 10;
            Assert.assertEquals(0x07, BitUtil.writeBit(source, position, bitValue));
        }
        
        [Test]
        public function testFlipBit() : void
        {
            source = parseInt("00100",2);
            position = 2;
            Assert.assertEquals(0, BitUtil.flipBit(source, position));
            
            source = parseInt("00101",2);
            position = 0;
            Assert.assertEquals(4, BitUtil.flipBit(source, position));
        }
    }
}
