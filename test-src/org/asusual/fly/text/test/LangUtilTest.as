// Copyright 2012. Art. Lebedev Studio. All Rights Reserved.
package org.asusual.fly.text.test
{
    import org.flexunit.Assert;
    import org.asusual.fly.text.LangUtil;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class LangUtilTest
    {

        [Test]
        public function numberWordTest314():void
        {
            var word:String = LangUtil.numberWord(314, "день", "дня", "дней");
            Assert.assertEquals("дней", word);
        }

        [Test]
        public function numberWordTest54():void
        {
            var word:String = LangUtil.numberWord(54, "день", "дня", "дней");
            Assert.assertEquals("дня", word);
        }

        [Test]
        public function numberWordTest82():void
        {
            var word:String = LangUtil.numberWord(82, "день", "дня", "дней");
            Assert.assertEquals("дня", word);
        }

        [Test]
        public function numberWordTest311():void
        {
            var word:String = LangUtil.numberWord(311, "день", "дня", "дней");
            Assert.assertEquals("дней", word);
        }
        
        [Test]
        public function testFormatNumber() : void {
        	var num : Number = 4000000;
            Assert.assertEquals("4 000 000", LangUtil.formatNumber(num, 3));
        }
        
        [Test]
        public function testFormatNumber2() : void {
        	var num : Number = 8899334400;
            Assert.assertEquals("88 99 33 44 00", LangUtil.formatNumber(num, 2));
        }
        
        [Test]
        public function testFormatNumber3() : void {
        	var num : Number = 100;
            Assert.assertEquals("100", LangUtil.formatNumber(num, 3));
        }
    }
}
