package org.asusual.fly.object {
    import org.flexunit.Assert;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class ObjectUtilTest {
        [Test]
        public function testCopy():void
        {
            var word:Object = {hello:"World", foo:4, color:0xFFFFFF};
            var copy:Object = ObjectUtil.copy(word);
            Assert.assertEquals(copy["hello"], "World");
            Assert.assertEquals(copy["foo"], 4);
            Assert.assertEquals(copy["color"], 0xFFFFFF);
        }
    }
}
