package org.asusual.fly.graphics.test {
    import org.asusual.fly.graphics.HoverMaker;
    import flexunit.framework.Assert;
    import org.fluint.uiImpersonation.UIImpersonator;
    import flash.display.Sprite;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class HoverMakerTest {
        private var sprite : Sprite;
        
        [Before]
        public function beforeTest() : void {
        	sprite = new Sprite();
            sprite.graphics.beginFill(0xFF0000);
            sprite.graphics.drawRect(0, 0, 100, 100);
            sprite.graphics.endFill();
            UIImpersonator.addChild(sprite);
        }
        
        [Test]
        public function testRegister() : void {
        	Assert.assertTrue(HoverMaker.register(sprite));
        }
        
        [Test]
        public function testUnregister() : void {
            HoverMaker.register(sprite);
        	Assert.assertTrue(HoverMaker.unregister(sprite));
        }
        
        [After]
        public function afterTest() : void {
        	UIImpersonator.removeChild(sprite);
            sprite = null;
        }
    }
}
