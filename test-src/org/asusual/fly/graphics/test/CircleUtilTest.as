package org.asusual.fly.graphics.test {
    import org.asusual.fly.graphics.CircleUtil;
    import flash.display.Sprite;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class CircleUtilTest {
        
        [Test(expects="Error")]
        public function testEndTime() : void {
            var sprite : Sprite = new Sprite();
            CircleUtil.drawSector(sprite, 30, 2);
        }
        
        [Test(expects="Error")]
        public function testEndTime2() : void {
            var sprite : Sprite = new Sprite();
            CircleUtil.drawSector(sprite, 30, -2);
        }
        
        [Test(expects="Error")]
        public function testStep() : void {
            var sprite : Sprite = new Sprite();
            CircleUtil.drawSector(sprite, 30, 0.4, -0.3);
        }
    }
}
