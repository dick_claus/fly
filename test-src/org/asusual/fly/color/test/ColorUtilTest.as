// Copyright 2012. Art. Lebedev Studio. All Rights Reserved.
package org.asusual.fly.color.test {
    import flash.display.Sprite;
    import org.flexunit.Assert;
    import org.asusual.fly.color.ColorUtil;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class ColorUtilTest
    {

        [Test]
        public function testExtractRedChannel():void
        {
            var color:uint = 0xEF5533;
            var redChannel:uint = ColorUtil.extractRedChannel(color);
            Assert.assertEquals(0xEF, redChannel);
        }

        [Test]
        public function testExtractGreenChannel():void
        {
            var color:uint = 0xEF5533;
            var greenChannel:uint = ColorUtil.extractGreenChannel(color);
            Assert.assertEquals(0x55, greenChannel);
        }

        [Test]
        public function testExtractBlueChannel():void
        {
            var color:uint = 0xEF5533;
            var blueChannel:uint = ColorUtil.extractBlueChannel(color);
            Assert.assertEquals(0x33, blueChannel);
        }
        
        [Test]
        public function testTint():void
        {
            var sprite : Sprite = new Sprite();
            sprite.graphics.beginFill(0x00FF00);
            sprite.graphics.drawRect(0, 0, 100, 100);
            sprite.graphics.endFill();
            var color:uint = 0xEF5533;
            ColorUtil.tint(sprite, color);
            Assert.assertEquals(sprite.transform.colorTransform.color, color);
        }
        
        [Test]
        public function testRgbToHex(): void {
        	var r : int = 22;
            var g : int = 167;
            var b : int = 220;
            var color : uint = ColorUtil.rbgToHex(r, g, b);
            Assert.assertEquals(0x16a7dc, color);
        }
    }
}
