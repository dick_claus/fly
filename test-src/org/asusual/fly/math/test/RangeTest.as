package org.asusual.fly.math.test {
    import org.hamcrest.number.between;
    import org.flexunit.assertThat;
    import flexunit.framework.Assert;

    import org.asusual.fly.math.Range;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class RangeTest {
        
        [Test]
        public function testGetRandomNumber() : void
        {
            var low : Number = 1;
            var high : Number = 20;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomNumber();
            Assert.assertTrue("Error, random value is too big", random < high);
            Assert.assertTrue("Error, random value is too small", random >= low);
        }
        
        [Test]
        public function testGetRandomNumber2() : void
        {
            var low : Number = 1;
            var high : Number = 20;
            var range : Range = new Range(high, low);
            var random : Number = range.getRandomNumber();
            Assert.assertTrue("Error, random value is too big", random < high);
            Assert.assertTrue("Error, random value is too small", random >= low);
        }
        
        [Test]
        public function testGetRandomNumber3() : void
        {
            var low : Number = -1;
            var high : Number = 20;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomNumber();
            Assert.assertTrue("Error, random value is too big", random < high);
            Assert.assertTrue("Error, random value is too small", random >= low);    
        }
        
        [Test]
        public function testGetRandomNumber4() : void
        {
            var low : Number = -20;
            var high : Number = -1;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomNumber();
            Assert.assertTrue("Error, random value is too big", random < high);
            Assert.assertTrue("Error, random value is too small", random >= low);    
        }
        
        [Test]
        public function testGetRandomNumber5() : void
        {
            var low : Number = 3;
            var high : Number = 4;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomNumber();
            
            Assert.assertTrue("Error, random value is too big", random < high);
            Assert.assertTrue("Error, random value is too small", random >= low);    
        }
        
        [Test(expects="Error")]
        public function testGetRandomNumber6() : void
        {
            var low : Number = 3;
            var high : Number = 3;
            var range : Range = new Range(low, high);
            var unionInterval : Number = 0.5;
            var random : Number = range.getRandomNumber(unionInterval);
            
            Assert.assertTrue("Error, random value is too big", random < high);
            Assert.assertTrue("Error, random value is too small", random >= low);    
        }
        
        [Test]
        public function testGetRandomNumber7() : void
        {
            var low : Number = 1.2;
            var high : Number = 1.4;
            var range : Range = new Range(low, high);
            var unionInterval : Number = 0.2;
            var random : Number = range.getRandomNumber(unionInterval);
            
            Assert.assertTrue("Error, random value is too big", random < high);
            Assert.assertTrue("Error, random value is too small", random >= low);    
        }
        
        [Test]
        public function testGetRandomNumber8() : void
        {
            var low : Number = 0;
            var high : Number = 3.5;
            var range : Range = new Range(low, high);
            var unionInterval : Number = 0;
            var random : Number = range.getRandomNumber(unionInterval);
            
            Assert.assertTrue("Error, random value is too big", random < high);
            Assert.assertTrue("Error, random value is too small", random >= low);    
        }
        
        [Test]
        public function testGetRandomNumber9() : void
        {
            var low : Number = 0;
            var high : Number = 3.5;
            var range : Range = new Range(low, high);
            var unionInterval : Number = 0.2;
            var random : Number = range.getRandomNumber(unionInterval);
            
            Assert.assertTrue("Error, random value is too big", random < high);
            Assert.assertTrue("Error, random value is too small", random >= low);    
        }
        
        [Test]
        public function testGetRandomInt() : void
        {
            var low : Number = 8;
            var high : Number = 12;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt();
            Assert.assertTrue("Error, random value is too big", random <= high);
            Assert.assertTrue("Error, random value is too small", random >= low);
            assertThat(random, between(low, high));
        }
        
        [Test]
        public function testGetRandomInt2() : void
        {
            var low : Number = -5;
            var high : Number = 20;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt();
            Assert.assertTrue("Error, random value is too big", random <= high);
            Assert.assertTrue("Error, random value is too small", random >= low);
            assertThat(random, between(low, high));
        }
        
        [Test]
        public function testGetRandomInt3() : void
        {
            var low : Number = -5.5;
            var high : Number = 2.7;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt();
            Assert.assertTrue("Error, random value is too big", random <= high);
            Assert.assertTrue("Error, random value is too small", random >= low);
            assertThat(random, between(low, high));
        }
        
        [Test]
        public function testGetRandomInt4() : void
        {
            var low : Number = -5.5;
            var high : Number = 2.7;
            var unitInterval : Number = 0;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt(unitInterval);
            Assert.assertTrue("Error, random value is too big", random <= high);
            Assert.assertTrue("Error, random value is too small", random >= low);
            assertThat(random, between(low, high));
        }
        
        [Test]
        public function testGetRandomInt5() : void
        {
            var low : Number = -5.5;
            var high : Number = 2.7;
            var unitInterval : Number = 1;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt(unitInterval);
            Assert.assertTrue("Error, random value is too big", random <= high);
            Assert.assertTrue("Error, random value is too small", random >= low);
            assertThat(random, between(low, high));
            Assert.assertEquals(random, 2);
        }
        
        [Test]
        public function testGetRandomInt6() : void
        {
            var low : Number = -5.5;
            var high : Number = 2.7;
            var unitInterval : Number = -0.5;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt(unitInterval);
            Assert.assertTrue("Error, random value is too big", random <= high);
            Assert.assertTrue("Error, random value is too small", random >= low);
            assertThat(random, between(low, high));
        }
        
        [Test]
        public function testGetRandomInt7() : void
        {
            var low : Number = 3;
            var high : Number = 3;
            var unitInterval : Number = -0.5;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt(unitInterval);
            Assert.assertEquals(3, random);
            Assert.assertTrue("Error, random value is too big", random <= high);
            Assert.assertTrue("Error, random value is too small", random >= low);
            assertThat(random, between(low, high));
        }
        
        [Test]
        public function testGetRandomInt8() : void
        {
            var low : Number = 0;
            var high : Number = 3;
            var unitInterval : Number = 0;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt(unitInterval);
            Assert.assertTrue("Error, random value is too big", random <= high);
            Assert.assertTrue("Error, random value is too small", random >= low);
            assertThat(random, between(low, high));
        }
        
        [Test]
        public function testGetRandomInt9() : void
        {
            var low : Number = 0;
            var high : Number = 3;
            var unitInterval : Number = 0;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt(unitInterval);
            Assert.assertTrue("Error, random value is too big", random <= high);
            Assert.assertTrue("Error, random value is too small", random >= low);
            assertThat(random, between(low, high));
        }
        
        [Test]
        public function testGetRandomInt10() : void
        {
            var low : Number = 1;
            var high : Number = 2;
            var unitInterval : Number = 0.7;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt(unitInterval);
            Assert.assertEquals(high, random);
            assertThat(random, between(low, high));
        }
        
        [Test]
        public function testGetRandomInt11() : void
        {
            var low : Number = 1;
            var high : Number = 3;
            var unitInterval : Number = 0.5;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt(unitInterval);
            Assert.assertEquals(random,2);
            assertThat(random, between(low, high));
        }
        
        [Test]
        public function testGetRandomInt12() : void
        {
            var low : Number = 1;
            var high : Number = 2;
            var unitInterval : Number = 1;
            var range : Range = new Range(low, high);
            var random : Number = range.getRandomInt(unitInterval);
            Assert.assertEquals(random,2);
            assertThat(random, between(low, high));
        }
        
        [Test(expects="RangeError")]
        public function testGetFractionFromNumber01() : void {
            var number : Number = 20;
            var range : Range = new Range(100, 300);
            range.getFractionFromNumber(number);
        }
        
        [Test]
        public function testGetFractionFromNumber02() : void {
        	var number : Number = 50;
            var range : Range = new Range(0, 100);
            Assert.assertEquals(0.5, range.getFractionFromNumber(number));
        }
        
        [Test]
        public function testGetFractionFromNumber03() : void {
        	var number : Number = 0;
            var range : Range = new Range(-100, 100);
            Assert.assertEquals(0.5, range.getFractionFromNumber(number));
        }
        
        [Test]
        public function testGetFractionFromNumber04() : void {
        	var number : Number = 1.3;
            var range : Range = new Range(1, 2);
            Assert.assertEquals(0.3, range.getFractionFromNumber(number));
        }
        
        [Test]
        public function testGetFractionFromNumber05() : void {
        	var number : Number = 6.3;
            var range : Range = new Range(6.2, 6.4);
            Assert.assertEquals(0.5, range.getFractionFromNumber(number));
        }
        
        [Test]
        public function testGetFractionFromNumber06() : void {
        	var number : Number = 10;
            var range : Range = new Range(10, 60);
            Assert.assertEquals(0, range.getFractionFromNumber(number));
        }
        
        [Test]
        public function testGetFractionFromNumber07() : void {
        	var number : Number = 60;
            var range : Range = new Range(10, 60);
            Assert.assertEquals(1, range.getFractionFromNumber(number));
        }
        
        [Test(expects="RangeError")]
        public function testGetNumberFromFraction01() : void {
            var fraction : Number = -1;
            var range : Range = new Range(100, 300);
            range.getNumberFromFraction(fraction);
        }
        
        [Test]
        public function testGetNumberFromFraction02() : void {
        	var fraction : Number = 0.5;
            var range : Range = new Range(0, 100);
            Assert.assertEquals(50, range.getNumberFromFraction(fraction));
        }
        
        [Test]
        public function testGetNumberFromFraction03() : void {
        	var fraction : Number = 0.5;
            var range : Range = new Range(-100, 100);
            Assert.assertEquals(0, range.getNumberFromFraction(fraction));
        }
        
        [Test]
        public function testGetNumberFromFraction04() : void {
        	var fraction : Number = 0.5;
            var range : Range = new Range(6.2, 6.4);
            Assert.assertEquals(6.3, range.getNumberFromFraction(fraction));
        }
        
        [Test]
        public function testGetNumberFromFraction05() : void {
        	var fraction : Number = 0.3;
            var range : Range = new Range(1, 2);
            Assert.assertEquals(1.3, range.getNumberFromFraction(fraction));
        }
        
        [Test]
        public function testGetNumberFromFraction06() : void {
        	var fraction : Number = 0;
            var range : Range = new Range(10, 60);
            Assert.assertEquals(10, range.getNumberFromFraction(fraction));
        }
        
        [Test]
        public function testGetNumberFromFraction07() : void {
        	var fraction : Number = 1;
            var range : Range = new Range(10, 60);
            Assert.assertEquals(60, range.getNumberFromFraction(fraction));
        }
        
        [Test]
        public function testPutInside01() : void {
        	var num : Number = 1;
            var range : Range = new Range(10, 60);
            Assert.assertEquals(10, range.putInside(num));
        }
        
        [Test]
        public function testPutInside02() : void {
        	var num : Number = 100;
            var range : Range = new Range(-99, 60);
            Assert.assertEquals(60, range.putInside(num));
        }
        
    }
}
