package org.asusual.fly.math.test {
    import org.asusual.fly.math.StatisticsUtil;
    import flexunit.framework.Assert;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class StatisticsUtilTest {
        
        [Test]
        public function averageTest() : void {
        	var array : Array = [2, 3, 4, 5, 6];
            Assert.assertEquals(4, StatisticsUtil.average(array));
        }
        
        [Test]
        public function sumTest() : void {
        	var array : Array = [2, 3, 4, 5, 6];
            Assert.assertEquals(20, StatisticsUtil.sum(array));
        }
    }
}
