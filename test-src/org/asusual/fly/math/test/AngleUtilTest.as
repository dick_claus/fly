package org.asusual.fly.math.test {
    import org.asusual.fly.math.AngleUtil;
    import flexunit.framework.Assert;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class AngleUtilTest {
        
        [Test]
        public function testDegToRad1() : void
        {
            Assert.assertEquals(Math.PI/2,AngleUtil.angleToRad(90));
        }
        
        [Test]
        public function testDegToRad2() : void
        {
            Assert.assertEquals(Math.PI,AngleUtil.angleToRad(180));
        }
        
        [Test]
        public function testDegToRad3() : void
        {
            Assert.assertEquals(-Math.PI/2*3,AngleUtil.angleToRad(-270));
        }
        
        [Test]
        public function testRadToDeg1() : void
        {
            Assert.assertEquals(45,AngleUtil.radToAngle(Math.PI/4));
        }
        
        [Test]
        public function testRadToDeg2() : void
        {
            Assert.assertEquals(-30,AngleUtil.radToAngle(-Math.PI/6));
        }
        
        [Test]
        public function testRadToDeg3() : void
        {
            Assert.assertEquals(360,AngleUtil.radToAngle(Math.PI*2));   
        }
        
        
        
    }
}
