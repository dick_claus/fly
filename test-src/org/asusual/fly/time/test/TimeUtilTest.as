package org.asusual.fly.time.test {
import org.asusual.fly.menu.MenuItem;
import org.asusual.fly.time.TimeUtil;
import org.flexunit.Assert;

public class TimeUtilTest {

        [Test]
        public function testGetDaysBackToday() : void
        {
            var now:Number = 14500000;
            var last:Number = 14003600;
            var text:String = TimeUtil.getDaysBack(last, now);
            Assert.assertEquals("сегодня", text);
        }

        [Test]
        public function testGetDaysBackYesterday() : void
        {
            var now:Number = 88800000;
            var last:Number = 1003600;
            var text:String = TimeUtil.getDaysBack(last, now);
            Assert.assertEquals("вчера", text);
        }

        [Test(expects="Error")]
        public function testGetDaysBackFutureError() : void
        {
            var now:Number = 81000;
            var last:Number = 14003600;
            var text:String = TimeUtil.getDaysBack(last, now);
            Assert.assertEquals("сегодня", text);
        }

        [Test]
        public function testGetNaturalDayDiff4Days() : void
        {
            var now:Number = 500 + TimeUtil.DAY_MS*4;
            var last:Number = 500;
            var text:String = TimeUtil.getDaysBack(last, now);
            Assert.assertEquals("4 дня назад", text);
        }

        [Test]
        public function testGetNaturalDayDiff21Days() : void
        {
            var now:Number = 500 + TimeUtil.DAY_MS*21;
            var last:Number = 500;
            var text:String = TimeUtil.getDaysBack(last, now);
            Assert.assertEquals("21 день назад", text);
        }

        [Test]
        public function testGetNaturalDayDiff17Days() : void
        {
            var now:Number = 500 + TimeUtil.DAY_MS*17;
            var last:Number = 500;
            var text:String = TimeUtil.getDaysBack(last, now);
            Assert.assertEquals("17 дней назад", text);
        }


    }
}
