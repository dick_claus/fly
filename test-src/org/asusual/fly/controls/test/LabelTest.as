package org.asusual.fly.controls.test {
    import org.asusual.fly.controls.Label;
    import org.flexunit.Assert;
    /**
     * @author Dmitry.Bezverkhiy
     */
    public class LabelTest {
        
        [Test]
        public function setTextConstructor() : void {
            var label : Label = new Label("Yes");            
            Assert.assertEquals("Yes", label.text);
        }
        
        
        [Test]
        public function setText() : void {
            var label : Label = new Label(null);
            label.text = "Yes";
            Assert.assertEquals("Yes", label.text);
        }        
        
    }
}
