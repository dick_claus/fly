package org.asusual.fly.controls.test {
	import org.asusual.fly.controls.Button;
	import org.flexunit.Assert;
	/**
	 * @author Dmitry.Bezverkhiy
	 */
	public class ButtonTest {
		
		
		[Test]
		public function testSetLabelText() : void {
			var button : Button = new Button("Test");
			button.labelText = "OK";
			Assert.assertEquals("OK", button.labelText);
		}
	}
}
