package org.asusual.fly.utils.test {
    import org.flexunit.Assert;
    import org.asusual.fly.utils.GUID;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class GUIDTest {
        [Test]
        public function testCreate() : void {
        	var id : String = GUID.create();
            Assert.assertNotNull(id);
        }
    }
}
