package org.asusual.fly.screen.test {
    import org.asusual.fly.screen.Screen;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class TestScreen extends Screen {
        public function TestScreen() {
            super();
            this.graphics.beginFill(0xFF0000);
            this.graphics.drawRect(0, 0, 10, 10);
            this.graphics.endFill();
        }
        
        public function testChangeScreen() : void {
        	changeScreen(ScreensTest.WELCOME_SCREEN);
        }
    }
}
