package org.asusual.fly.screen.test {
    import org.hamcrest.Matcher;
    import org.hamcrest.object.IsInstanceOfMatcher;
    import org.asusual.fly.screen.Screen;
    import org.hamcrest.object.equalTo;
    import org.hamcrest.assertThat;

    import flexunit.framework.Assert;

    import org.fluint.uiImpersonation.UIImpersonator;
    import org.asusual.fly.screen.ScreenManager;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class ScreenManagerTest {
        private var screenManager : ScreenManager;

        [Before]
        public function beforeTest() : void {
            screenManager = new ScreenManager();

            UIImpersonator.addChild(screenManager);
        }

        [Test]
        public function testRegisterScreen() : void {
            var screens : Object = screenManager.registerScreen(WelcomeScreen, ScreensTest.WELCOME_SCREEN);
            Assert.assertNotNull(screens[ScreensTest.WELCOME_SCREEN]);
        }
        
        [Test]
        public function testGetActiveScreen() : void {
            screenManager.registerScreen(WelcomeScreen, ScreensTest.WELCOME_SCREEN);
            screenManager.registerScreen(TestScreen, ScreensTest.TEST_SCREEN);
            
            screenManager.changeScreen(ScreensTest.TEST_SCREEN);
            var activeScreen : Screen = screenManager.getActiveScreen();
            Assert.assertNotNull(activeScreen);
            (activeScreen as TestScreen).testChangeScreen();
            var matcher : Matcher = new IsInstanceOfMatcher(WelcomeScreen);
            assertThat(matcher.matches(screenManager.getActiveScreen()));
        }

        [Test]
        public function testChangeScreen() : void {
            screenManager.registerScreen(WelcomeScreen, ScreensTest.WELCOME_SCREEN);
            screenManager.changeScreen(ScreensTest.WELCOME_SCREEN);
            var index : int = UIImpersonator.getChildIndex(screenManager);
            assertThat((UIImpersonator.getChildAt(index) as ScreenManager).numChildren, equalTo(1));
        }
        
        [Test]
        public function testChangeScreenMore() : void {
            screenManager.registerScreen(WelcomeScreen, ScreensTest.WELCOME_SCREEN);
            screenManager.registerScreen(WelcomeScreen, ScreensTest.TEST_SCREEN);
            screenManager.changeScreen(ScreensTest.TEST_SCREEN);
            
            
            var index : int = UIImpersonator.getChildIndex(screenManager);
            assertThat((UIImpersonator.getChildAt(index) as ScreenManager).numChildren, equalTo(1));
        }

        [After]
        public function afterTest() : void {
            UIImpersonator.removeChild(screenManager);
        }
    }
}
