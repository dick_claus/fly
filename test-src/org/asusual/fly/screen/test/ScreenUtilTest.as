package org.asusual.fly.screen.test {
    import org.asusual.fly.screen.ScreenUtilScaleMode;
    import org.asusual.fly.screen.ScreenUtil;
    import org.flexunit.Assert;
    import org.fluint.uiImpersonation.UIImpersonator;

    import flash.display.Graphics;
    import flash.display.Sprite;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class ScreenUtilTest {
        private var wideSprite : Sprite;
        private var narrowSprite : Sprite;
        private var smallSprite : Sprite;
        private var bigSprite : Sprite;
        private var screen : Sprite;
        private var scaleWidthSprite : Sprite;
        private var scaleHeightSprite : Sprite;
        private var iphone5 : Sprite;
        private var game : Sprite;
        private var iphone4 : Sprite;
        
        [Before]
        public function beforeTest() : void {
            
            screen = new Sprite();
            drawRect(screen.graphics, 200, 200);
            
            wideSprite = new Sprite();
            drawRect(wideSprite.graphics, 1000, 200);
            
            narrowSprite = new Sprite();
            drawRect(narrowSprite.graphics, 200, 1000);
            
            smallSprite = new Sprite();
            drawRect(smallSprite.graphics, 100, 100);
            
            bigSprite = new Sprite();
            drawRect(bigSprite.graphics, 1000, 1000);
            
            scaleWidthSprite = new Sprite();
            drawRect(scaleWidthSprite.graphics, 400, 600);
            
            scaleHeightSprite = new Sprite();
            drawRect(scaleHeightSprite.graphics, 600, 400);
            
            iphone5 = new Sprite();
            drawRect(iphone5.graphics, 640, 1136);
            
            iphone4 = new Sprite();
            drawRect(iphone4.graphics, 640, 960);
            
            game = new Sprite();
            drawRect(game.graphics, 320, 480);
            
            UIImpersonator.addChild(screen);
            UIImpersonator.addChild(wideSprite);
            UIImpersonator.addChild(narrowSprite);
            UIImpersonator.addChild(smallSprite);
            UIImpersonator.addChild(bigSprite);
            UIImpersonator.addChild(scaleWidthSprite);
            UIImpersonator.addChild(scaleHeightSprite);
            UIImpersonator.addChild(game);
            UIImpersonator.addChild(iphone5);
            UIImpersonator.addChild(iphone4);
        }
        
        public function drawRect(graphics : Graphics, w : Number, h : Number) : void {
            graphics.beginFill(0xFFFFFF*Math.random());
            graphics.drawRect(0, 0, w, h);
            graphics.endFill();
        }
        
        [Test]
        public function testWideSprite() : void {
            ScreenUtil.centerToScreen(screen, wideSprite);
            Assert.assertEquals(-400,wideSprite.x);
        }

        [Test]        
        public function testNarrowSprite() : void {
            ScreenUtil.centerToScreen(screen, narrowSprite);
            Assert.assertEquals(-400, narrowSprite.y);
        }

        [Test]
        public function testSmallSprite() : void {
            ScreenUtil.centerToScreen(screen, smallSprite);
            Assert.assertEquals(50, smallSprite.x);
            Assert.assertEquals(50, smallSprite.y);
        }
        
        [Test]
        public function testBigSprite() : void {
            ScreenUtil.centerToScreen(screen, bigSprite);
            Assert.assertEquals(-400, bigSprite.x);
            Assert.assertEquals(-400, bigSprite.y);
        }
        
        [Test]
        public function testWidthScale() : void {
            ScreenUtil.fitToScreen(screen, scaleWidthSprite, ScreenUtilScaleMode.ZOOM);
            Assert.assertEquals(200, scaleWidthSprite.width);
            Assert.assertEquals(300, scaleWidthSprite.height);
        }
        
        [Test]
        public function testHeightScale() : void {
            ScreenUtil.fitToScreen(screen, scaleHeightSprite, ScreenUtilScaleMode.ZOOM);
            Assert.assertEquals(300, scaleHeightSprite.width);
            Assert.assertEquals(200, scaleHeightSprite.height);
        }
        
        [Test]
        public function testCentralVer() : void {
            ScreenUtil.alignVerticalCenter(screen, smallSprite);
            Assert.assertEquals(50, smallSprite.y);
        }
        
        [Test]
        public function testCentralVer2() : void {
            ScreenUtil.alignVerticalCenter(screen, bigSprite);
            Assert.assertEquals(-400, bigSprite.y);
        }
        
        [Test]
        public function testCentralHor() : void {
            ScreenUtil.alignHorizontalCenter(screen, smallSprite);
            Assert.assertEquals(50, smallSprite.x);
        }
        
        [Test]
        public function testCentralHor2() : void {
            ScreenUtil.alignHorizontalCenter(screen, bigSprite);
            Assert.assertEquals(-400, bigSprite.x);
        }
        
        [Test]
        public function testScaleModeLetterBox() : void {
            ScreenUtil.fitToScreen(iphone4, game, ScreenUtilScaleMode.LETTERBOX);
            Assert.assertEquals(640, game.width);
            Assert.assertEquals(960, game.height);
        }
        
        [Test]
        public function testScaleModeLetterBox1() : void {
            ScreenUtil.fitToScreen(iphone5, game, ScreenUtilScaleMode.LETTERBOX);
            Assert.assertEquals(640, game.width);
            Assert.assertEquals(960, game.height);
        }
        
        [Test]
        public function testScaleModeZoom() : void {
            ScreenUtil.fitToScreen(iphone5, game, ScreenUtilScaleMode.ZOOM);
            Assert.assertEquals(757, game.width);
            Assert.assertEquals(1136, game.height);
        }
        
        [After]
        public function afterTest() : void {
            UIImpersonator.removeChild(screen);
            UIImpersonator.removeChild(wideSprite);
            UIImpersonator.removeChild(narrowSprite);
            UIImpersonator.removeChild(smallSprite);
            UIImpersonator.removeChild(bigSprite);
            UIImpersonator.removeChild(scaleWidthSprite);
            UIImpersonator.removeChild(scaleHeightSprite);
            UIImpersonator.removeChild(game);
            UIImpersonator.removeChild(iphone5);
            UIImpersonator.removeChild(iphone4);
        }
    }
}
