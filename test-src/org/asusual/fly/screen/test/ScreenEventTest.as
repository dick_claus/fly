package org.asusual.fly.screen.test {
    import org.asusual.fly.screen.ScreenEvent;
    import org.flexunit.Assert;

    import flash.events.EventDispatcher;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class ScreenEventTest {
        [Test]
        public function testScreenId() : void {
        	var screenEvent : ScreenEvent = new ScreenEvent(ScreenEvent.CHANGE_SCREEN, ScreensTest.WELCOME_SCREEN);
            var dispatcher : EventDispatcher = new EventDispatcher();
            dispatcher.addEventListener(ScreenEvent.CHANGE_SCREEN, function (event : ScreenEvent) : void  {
                Assert.assertEquals(event.screenId, "welcomeScreen");
            });
            
            dispatcher.dispatchEvent(screenEvent);
        }
        
    }
}
