package org.asusual.fly.screen.test {
    import org.fluint.uiImpersonation.UIImpersonator;
    import org.asusual.fly.screen.ScreenManager;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class ScreenTest {
        private var screenManager : ScreenManager;
        
        [Before]
        public function beforeTest() : void {
        	screenManager = new ScreenManager();
            
            screenManager.registerScreen(TestScreen, ScreensTest.TEST_SCREEN);
            
            UIImpersonator.addChild(screenManager);
        }
        
        [After]
        public function afterTest() : void {
            UIImpersonator.removeChild(screenManager);
            screenManager = null;
        }
    }
}
