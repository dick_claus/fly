package org.asusual.fly.screen.test {
    import org.asusual.fly.screen.Screen;

    /**
     * @author Dmitry Bezverkhiy
     */
    public class WelcomeScreen extends Screen {
        public function WelcomeScreen() {
            super();
            this.graphics.beginFill(0);
            this.graphics.drawRect(0, 0, 10, 10);
            this.graphics.endFill();
        }
    }
}
