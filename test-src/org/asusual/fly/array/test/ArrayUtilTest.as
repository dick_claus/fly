package org.asusual.fly.array.test {
    import org.hamcrest.assertThat;
    import flexunit.framework.Assert;

    import org.asusual.fly.array.ArrayUtil;
    import org.hamcrest.collection.hasItem;
    import org.hamcrest.core.allOf;
    import org.hamcrest.number.closeTo;
    import org.hamcrest.object.equalTo;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class ArrayUtilTest {
        
        [Test]
        public function testShuffle() : void
        {
            var input : Array = [1,2,3,4,5,6,7,8];
            var output : Array = ArrayUtil.shuffle(input);
            assertThat(output, allOf(hasItem(equalTo(3)), hasItem(closeTo(1, 8))));
        }
        
        [Test]
        public function testCopyForNull() : void
        {
            var input : Array = null;
            var output : Array = ArrayUtil.copy(input);
            Assert.assertNotNull(output);
        }
        
        [Test]
        public function testCopyForEqual() : void
        {
            var input : Array = [1,2,3,4,5,6,7,8];
            var output : Array = ArrayUtil.copy(input);
            assertThat(output, equalTo(input));
        }
        
        
    }
}
