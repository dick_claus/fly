package org.asusual.fly.menu.test {
    import org.asusual.fly.menu.MenuItem;
    import org.asusual.fly.menu.MenuItemEvent;
    import org.flexunit.Assert;
    import org.flexunit.async.Async;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class MenuItemTest {
        
        [Test]
        public function testMenuItemID() : void {
        	var menuItem : MenuItem = new MenuItem('first');
            Assert.assertEquals('first', menuItem.id);
        }
        
        [Test]
        public function testSelected() : void {
        	var menuItem : MenuItem = new MenuItem("test");
            menuItem.select();
            Assert.assertTrue(menuItem.selected);
        }
        
        [Test(async)]
        public function testSelectNoEvent() : void {
        	var menuItem : MenuItem = new MenuItem("1");
            menuItem.addEventListener(MenuItemEvent.ITEM_SELECTED, Async.asyncHandler(this, onItemSelectedNoEvent,100));
            menuItem.select(true);
        }

        public function onItemSelectedNoEvent(event : MenuItemEvent, passthrought : Object) : void {
            Assert.assertTrue(event.noEvent);
        }
        
        [Test]
        public function testDeslect() : void {
            var menuItem : MenuItem = new MenuItem('2');
            menuItem.select();
            menuItem.deselect();
            Assert.assertFalse(menuItem.selected);
        }
        
    }
}
