package org.asusual.fly.menu.test {
    import org.asusual.fly.menu.MenuItem;
    import org.flexunit.Assert;
    import org.asusual.fly.menu.Menu;
    /**
     * @author Dmitry Bezverkhiy
     */
    public class MenuTest {
        private var menu : Menu;
        
        [Before]
        public function beforeTest() : void {
        	menu = new Menu();
            var menuItem : MenuItem = new MenuItem('first');
            var menuItem2 : MenuItem = new MenuItem('second');
            menu.addItem(menuItem);
            menu.addItem(menuItem2);
        }
        
        [Test]
        public function testMenuCreate() : void {
        	var menu : Menu = new Menu();
            Assert.assertNotNull(menu);
        }
        
        [Test]
        public function testAddItem() : void {
            var menuItem : MenuItem = new MenuItem('third');
            var items : Array = menu.addItem(menuItem);
            Assert.assertEquals(3, items.length);
        }
        
        [Test]
        public function testClearMenu() : void {
        	var items : Array = menu.clearMenu();
            Assert.assertEquals(0, items.length);
        }
        
        [Test]
        public function testGetItemById() : void {
        	var item : MenuItem = menu.getItemById('first');
            Assert.assertNotNull(item);
        }
        
        [Test]
        public function testGetItemById2() : void {
        	var item : MenuItem = menu.getItemById('third');
            Assert.assertNull(item);
        }
        
        
        [Test]
        public function testSelect() : void {
        	menu.select('first');
            var menuItem : MenuItem = menu.getItemById('first');
            Assert.assertTrue(menuItem.selected);
        }
        
        [Test]
        public function testDeselect() : void {
        	menu.select('second');
            var menuItem : MenuItem = menu.getItemById('first');
            Assert.assertFalse(menuItem.selected);
        }
        
        [After]
        public function afterTest() : void {
        	var items : Array = menu.clearMenu();
            Assert.assertEquals(0, items.length);
        }
    }
}
